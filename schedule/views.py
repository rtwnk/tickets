from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import loader
from django.core.exceptions import ObjectDoesNotExist
from models import *
from time import gmtime, strftime
from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import loader
from django.core.exceptions import ObjectDoesNotExist
from models import *
from time import gmtime, strftime
from forms import *

def main(request):
    if request.method == 'POST':
        if 'searchbtn' in request.POST:
            template = loader.get_template('schedule/main.html')
            cred = SearchByCredentialsForm()
            context = {'routelist': getallitems(request.POST['credentials']), 'credform': cred}
            return HttpResponse(template.render(context, request))
    template = loader.get_template('schedule/main.html')
    cred = SearchByCredentialsForm()
    context = {'routelist': getallitems(""), 'credform': cred}
    return HttpResponse(template.render(context, request))

def orderitem(request, itemid):
    if request.method == 'POST':
        if 'orderitem' in request.POST:
            name = request.POST['name']
            surname = request.POST['surname']
            email = request.POST['email']
            camera = Route.objects.get(id=itemid)
            Order.objects.create(name=name, surname = surname,email=email, ticket=camera)
            return HttpResponseRedirect('/tickets')
    context = {'item':Route.objects.get(id=itemid),'orderform':OrderForm()}
    template = loader.get_template('schedule/order_item.html')
    return HttpResponse(template.render(context, request))

def getallitems(credentials):
    if not credentials:
        items = [Route(x) for x in Route.objects.all()]
        listitems = [list(x) for x in Route.objects.all().values_list()]
    else:
        listitems = []
        # items = [x for x in Route.objects.filter(title__search=credentials)]
        # listitems = [list(x) for x in Route.objects.filter(title__search=credentials).values_list()]

    for i in listitems:
        i[1] = Location.objects.get(id = i[1]).city
        i[2] =  Location.objects.get(id = i[2]).city
        i[3] = Train.objects.get(id = i[3]).number
        print i
    return listitems
    
