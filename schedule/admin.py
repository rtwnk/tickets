from django.contrib import admin
from .models import Train, Location, Route, Order

admin.site.register(Train)
admin.site.register(Location)
admin.site.register(Route)
admin.site.register(Order)