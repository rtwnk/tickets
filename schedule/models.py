from __future__ import unicode_literals

from django.db import models


class Location(models.Model):
    city = models.CharField(max_length = 50, default="")  
    def __str__(self):
        return str(self.city) + ""


class Train(models.Model):
    number = models.CharField(max_length = 50, default="")  
    def __str__(self):
        return str(self.number) + ""

class Route(models.Model):
    from_location = models.ForeignKey(Location,null=True,related_name='from_location') 
    to_location = models.ForeignKey(Location,null=True,related_name='to_location') 
    train_id = models.ForeignKey(Train,on_delete=models.CASCADE) 
    from_date = models.DateTimeField() 
    to_date = models.DateTimeField()  
    def __str__(self):
        return "from at " + str(self.from_date) + " to at " +  str(self.to_date)

class Order(models.Model):
    name = models.CharField(max_length=100, default = "")
    surname = models.CharField(max_length=100, default = "")
    email = models.CharField(max_length=100, default = "") 
    ticket = models.ForeignKey(Route,on_delete=models.CASCADE, default = None)
    def __str__(self):
        return self.name + " " + self.surname + " "